package com.zhanghe.study.mybatis.mapper;

import com.zhanghe.study.mybatis.interceptors.Page;
import com.zhanghe.study.mybatis.interceptors.PageParam;
import com.zhanghe.study.mybatis.mapper.provider.StudentProvider;
import com.zhanghe.study.mybatis.model.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author zh
 * @date 2021/4/14 18:30
 */
public interface StudentMapper {

    @Insert("insert  into student (name,class_id) values (#{name},#{classes.id})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int insertStudent(Student student);

    @Update("update student set name = #{name} where id = #{id}")
    int updateStudent(Student student);

    @Delete("delete from student where id = #{id}")
    int deleteStudent(int id);

    @Select("select id,name from student where id = #{id}")
    Student getById(int id);

    @Select("select * from student where id = #{id}")
    @Results(
            {
                    @Result(id = true,column = "id",property = "id"),
                    @Result(column = "name",property = "name")
            }
    )
//    @ResultMap("com.zhanghe.study.mybatis.mapper.StudentMapper.baseResultMap")
    Student get(int id);

    @Select("select * from student where id = #{id}")
//    @Results(
//            {
//                    @Result(id = true,property = "id",column = "id"),
//                    @Result(property = "name",column = "name"),
//                    @Result(property = "classes",column = "class_id",
//                            one = @One(select = "com.zhanghe.study.mybatis.mapper.ClassesMapper.getById"))
//            }
//
//    )
    @ResultMap("com.zhanghe.study.mybatis.mapper.StudentMapper.baseResultMap")
    Student getAssociationById(int id);

    @Select("select * from student where class_id = #{classId}")
    @Results(
            {
                    @Result(id = true,column = "id",property = "id"),
                    @Result(column = "name",property = "name")
            }
    )
    List<Student> getByClassId(int classId);

    @Select("select * from student where class_id = #{classId}")
    @Results(
            {
                    @Result(id = true,column = "id",property = "id"),
                    @Result(column = "name",property = "name")
            }
    )
    List<Student> getByClassIdByPager(@Param(value = "classId") int classId,@Param(value = "page") int page,@Param(value = "size") int size);

    @Select("select * from student where class_id = #{classId}")
    @Results(
            {
                    @Result(id = true,column = "id",property = "id"),
                    @Result(column = "name",property = "name")
            }
    )
    Page<Student> getByClassIdPage(@Param(value = "classId") int classId, @Param(value = "pageParam")PageParam pageParam);


    @SelectProvider(type = StudentProvider.class,method = "findById")
    @ResultMap("com.zhanghe.study.mybatis.mapper.StudentMapper.baseResultMap")
    Student getByIdUseProvider(int id);
}
