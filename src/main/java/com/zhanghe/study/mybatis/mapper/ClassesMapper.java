package com.zhanghe.study.mybatis.mapper;

import com.zhanghe.study.mybatis.model.Classes;
import org.apache.ibatis.annotations.*;

/**
 * @author zh
 * @date 2021/4/14 18:30
 */
public interface ClassesMapper {

    @Insert("insert  into classes (name) values (#{name})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int insertClasses(Classes classes);

    @Update("update classes set name = #{name} where id = #{id}")
    int updateClasses(Classes classes);

    @Delete("delete from classes where id = #{id}")
    int deleteClasses(int id);

    @Select("select id,name from classes where id = #{id}")
    Classes getById(int id);

    @Select("select * from classes where id = #{id}")
//    @Results(
//            {
//                    @Result(id = true,column = "id",property = "id"),
//                    @Result(column = "name",property = "name")
//            }
//    )
    @ResultMap("com.zhanghe.study.mybatis.mapper.ClassesMapper.baseResultMap")
    Classes get(int id);

    @Select("select * from classes where id = #{id}")
    @Results({
            @Result(id = true,column = "id",property = "id"),
            @Result(column = "name",property = "name"),
            @Result(property = "studentList",column = "id",
                    many = @Many(select = "com.zhanghe.study.mybatis.mapper.StudentMapper.getByClassId"))
    })
//    @ResultMap("com.zhanghe.study.mybatis.mapper.ClassesMapper.collectionResultMap")
    Classes getColectionById(int id);
}
