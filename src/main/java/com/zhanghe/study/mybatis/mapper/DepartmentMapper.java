package com.zhanghe.study.mybatis.mapper;

import com.zhanghe.study.mybatis.model.Department;

/**
 * @author zh
 * @date 2021/1/13 10:36
 */
public interface DepartmentMapper {

    Department selectDepById(Integer id);

    Department getDepAndEmps(Integer id);
}
