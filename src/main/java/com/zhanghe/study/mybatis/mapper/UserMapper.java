package com.zhanghe.study.mybatis.mapper;

import com.zhanghe.study.mybatis.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zh
 * @date 2021/1/11 17:20
 */
public interface UserMapper {

    User selectUser(Integer id);

    void addUser(User user);

    void updateUser(User user);

    void deleteUser(Integer id);

    void addUserReturnGeneratedKey(User user);

    User selectUserByIdAndName(@Param("id") int id,@Param("name") String name);

    User selectUserByCondition(User user);

    List<User> selectByIds(List<Integer> ids);

    User selectUserReturnResultMap(Integer id);
}
