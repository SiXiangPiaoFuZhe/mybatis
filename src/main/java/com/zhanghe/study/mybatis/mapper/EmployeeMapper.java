package com.zhanghe.study.mybatis.mapper;

import com.zhanghe.study.mybatis.model.Employee;

import java.util.List;

/**
 * @author zh
 * @date 2021/1/13 10:36
 */
public interface EmployeeMapper {

    Employee getEmployeeById(Integer id);

    Employee getEmpAndDep(Integer id);

    List<Employee> getEmployeeByDid(Integer did);

    List<Employee> getEmployeeByCondition(Employee employee);

    List<Employee> getEmployeeByConditionChoose(Employee employee);

    void updateEmployeeBySet(Employee employee);
}
