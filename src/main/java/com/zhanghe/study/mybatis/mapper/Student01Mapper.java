package com.zhanghe.study.mybatis.mapper;

import com.zhanghe.study.mybatis.model.Student01;
import com.zhanghe.study.mybatis.model.Student01Example;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Student01Mapper {
    long countByExample(Student01Example example);

    int deleteByExample(Student01Example example);

    int deleteByPrimaryKey(Integer id);

    int insert(Student01 record);

    int insertSelective(Student01 record);

    List<Student01> selectByExample(Student01Example example);

    Student01 selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Student01 record, @Param("example") Student01Example example);

    int updateByExample(@Param("record") Student01 record, @Param("example") Student01Example example);

    int updateByPrimaryKeySelective(Student01 record);

    int updateByPrimaryKey(Student01 record);
}