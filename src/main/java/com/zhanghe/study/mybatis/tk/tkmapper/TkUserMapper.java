package com.zhanghe.study.mybatis.tk.tkmapper;

import com.zhanghe.study.mybatis.tk.tkmodel.TkUser;
import tk.mybatis.mapper.common.Mapper;

/**
 *  需要继承通用Mapper提供的Mapper接口
 * @author zh
 * @date 2021/1/18 18:21
 */
public interface TkUserMapper extends Mapper<TkUser> {
}
