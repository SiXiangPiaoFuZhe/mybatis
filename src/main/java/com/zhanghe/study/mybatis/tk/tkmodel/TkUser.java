package com.zhanghe.study.mybatis.tk.tkmodel;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author zh
 * @date 2021/1/11 17:20
 */
// 使用通用mapper需要指定实体类所对应的表名
@Table(name="user")
public class TkUser implements Serializable {

    public static final long serialVersionUID =  1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "account")
    private double account;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAccount() {
        return account;
    }

    public void setAccount(double account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", account=" + account +
                '}';
    }
}
