package com.zhanghe.study.mybatis.tk.tkmodel;

import javax.persistence.*;

/**
 * 员工表
 * @author zh
 * @date 2021/1/13 10:22
 */
@Table(name = "employee")
public class TkEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "salary")
    private double salary;
    private TkDepartment department;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public TkDepartment getDepartment() {
        return department;
    }

    public void setDepartment(TkDepartment department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", department=" + department +
                '}';
    }
}
