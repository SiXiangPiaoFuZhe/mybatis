package com.zhanghe.study.mybatis.tk.tkmodel;

import javax.persistence.*;
import java.util.List;

/**
 * 部门表
 * @author zh
 * @date 2021/1/13 10:22
 */
@Table(name="department")
public class TkDepartment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;

    private List<TkEmployee> employeeList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TkEmployee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<TkEmployee> employeeList) {
        this.employeeList = employeeList;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", employeeList=" + employeeList +
                '}';
    }
}
