package com.zhanghe.study.mybatis.service;

import com.zhanghe.study.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zh
 * @date 2021/1/15 17:13
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public void print(){
        System.out.println(userMapper);
    }
}
