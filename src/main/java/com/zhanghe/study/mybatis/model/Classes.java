package com.zhanghe.study.mybatis.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author zh
 * @date 2021/4/14 18:24
 */
public class Classes implements Serializable {
    public static final long serialVersionUID =  1L;

    private int id;
    private String name;

    private List<Student> studentList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    @Override
    public String toString() {
        return "Classes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", studentList=" + studentList +
                '}';
    }
}
