package com.zhanghe.study.mybatis.model;

import java.io.Serializable;

/**
 * @author zh
 * @date 2021/4/14 18:23
 */
public class Student implements Serializable {
    public static final long serialVersionUID =  1L;

    private int id;
    private String name;

    private Classes classes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", classes=" + classes +
                '}';
    }
}
