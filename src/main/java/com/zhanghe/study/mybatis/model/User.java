package com.zhanghe.study.mybatis.model;

import org.apache.ibatis.type.Alias;

import java.io.Serializable;

/**
 * @author zh
 * @date 2021/1/11 17:20
 */
@Alias("user")
public class User implements Serializable {

    public static final long serialVersionUID =  1L;

    private int id;
    private String name;
    private double account;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAccount() {
        return account;
    }

    public void setAccount(double account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", account=" + account +
                '}';
    }
}
