package com.zhanghe.study.mybatis.interceptors;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author zh
 * @date 2023/6/12 18:28
 */
public class Page<E> extends ArrayList<E> {
    private static final long serialVersionUID = 1L;
    private PageParam paginator;

    public Page() {
    }

    public Page(final Collection<? extends E> c) {
        super(c);
    }

    public Page(final Collection<? extends E> c, final PageParam p) {
        super(c);
        this.paginator = p;
    }

    public Page(final PageParam p) {
        this.paginator = p;
    }

    public PageParam getPaginator() {
        return this.paginator;
    }

    public int getPageSize() {
        if (this.paginator != null) {
            return this.paginator.getLimit();
        }
        return 0;
    }

    public int getPageNo() {
        if (this.paginator != null) {
            return this.paginator.getPage();
        }
        return 0;
    }

    public int getTotalCount() {
        if (this.paginator != null) {
            return this.paginator.getTotalCount();
        }
        return 0;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (obj.getClass() != Page.class) {
            return false;
        }
        final Page<E> fobj = (Page<E>) obj;
        return this.paginator != null && this.paginator.equals(fobj.getPaginator());
    }

    @Override
    public int hashCode() {
        if (this.paginator != null) {
            return this.getPaginator().hashCode();
        }
        return super.hashCode();
    }
}
