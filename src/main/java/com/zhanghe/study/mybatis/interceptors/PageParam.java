package com.zhanghe.study.mybatis.interceptors;

import org.apache.ibatis.session.RowBounds;

import java.io.Serializable;

/**
 * @author zh
 * @date 2023/6/12 17:56
 */
public class PageParam extends RowBounds implements Serializable {
    private static final long serialVersionUID = 1L;
    private int totalCount;
    protected int page;
    protected int limit;

    public PageParam() {
        this.page = 1;
        this.limit = Integer.MAX_VALUE;
    }

    public PageParam(RowBounds rowBounds) {
        this.page = 1;
        this.limit = Integer.MAX_VALUE;
        if (rowBounds instanceof PageParam) {
            final PageParam pageBounds = (PageParam) rowBounds;
            this.page = pageBounds.page;
            this.limit = pageBounds.limit;
        } else {
            this.page = rowBounds.getOffset() / rowBounds.getLimit() + 1;
            this.limit = rowBounds.getLimit();
        }
    }

    public PageParam(final int limit) {
        this.page = 1;
        this.limit = limit;
    }

    public PageParam(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(final int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(final int page) {
        this.page = page;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setLimit(final int limit) {
        this.limit = limit;
    }


    public int getOffset() {
        if (this.page >= 1) {
            return (this.page - 1) * this.limit;
        }
        return 0;
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder("PageBounds{");
        sb.append("page=").append(this.page);
        sb.append(", limit=").append(this.limit);
        sb.append(", totalCount=").append(this.totalCount);
        sb.append('}');
        return sb.toString();
    }
}
