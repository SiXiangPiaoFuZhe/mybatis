package com.zhanghe.study.mybatis;

import com.zhanghe.study.mybatis.mapper.UserMapper;
import com.zhanghe.study.mybatis.model.User;
import com.zhanghe.study.mybatis.service.UserService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * mybatis、spring整合测试
 * @author zh
 * @date 2021/1/15 16:08
 */
public class MybatisSpringTest {

    private ApplicationContext context;

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void beanFactory(){
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
        sqlSessionFactory = context.getBean(SqlSessionFactory.class);
    }

    @Test
    public void testbeanAutoWired(){
        UserMapper userMapper = context.getBean(UserMapper.class);
        System.out.println(userMapper);
        UserService userService = context.getBean(UserService.class);
        userService.print();

    }

    @Test
    public void testTwoLevelCache(){
        SqlSession session = sqlSessionFactory.openSession();
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user = userMapper.selectUser(8);
        System.out.println(user);
        userMapper.updateUser(user);
        session.close();
        SqlSession session1 = sqlSessionFactory.openSession();

        UserMapper userMapper1 = session1.getMapper(UserMapper.class);
        User user1 = userMapper1.selectUser(8);
        System.out.println(user1);
        System.out.println(user == user1);

        session1.close();
    }

}
