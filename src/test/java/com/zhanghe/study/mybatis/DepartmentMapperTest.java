package com.zhanghe.study.mybatis;

import com.zhanghe.study.mybatis.mapper.DepartmentMapper;
import com.zhanghe.study.mybatis.model.Department;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import static com.zhanghe.study.mybatis.Main.createFactory;

/**
 * @author zh
 * @date 2021/1/14 09:01
 */
public class DepartmentMapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void createSqlSessionFactory(){
        sqlSessionFactory = createFactory();
    }

    @Test
    public void getDepAndEmps() {
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        DepartmentMapper departmentMapper = session.getMapper(DepartmentMapper.class);
        Department department = departmentMapper.getDepAndEmps(2);
        System.out.println(department);
        System.out.println(department.getEmployeeList());
        session.close();
    }
}