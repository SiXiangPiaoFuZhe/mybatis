package com.zhanghe.study.mybatis;

import com.zhanghe.study.mybatis.tk.tkmapper.TkUserMapper;
import com.zhanghe.study.mybatis.tk.tkmodel.TkUser;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 整合通用mapper测试
 * @author zh
 * @date 2021/1/18 18:23
 */
public class TkMapperTest {

    private ApplicationContext context;

    @Before
    public void beanFactory(){
        context = new ClassPathXmlApplicationContext("applicationContext-tk.xml");
    }

    @Test
    public void testSelect(){
        TkUserMapper tkUserMapper = context.getBean(TkUserMapper.class);
        TkUser user = tkUserMapper.selectByPrimaryKey(8);
        System.out.println(user);
    }
}
