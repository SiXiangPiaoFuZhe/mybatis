package com.zhanghe.study.mybatis.configtest;

import com.zhanghe.study.mybatis.model.User;
import org.apache.ibatis.session.Configuration;

/**
 * @author zh
 * @date 2021/4/13 17:07
 */
public class MapperTest {

    public void test(){
        Configuration configuration = new Configuration();
        configuration.addMapper(User.class);
    }
}
