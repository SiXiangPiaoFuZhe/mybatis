package com.zhanghe.study.mybatis.configtest;

import com.zhanghe.study.mybatis.model.User;
import org.apache.ibatis.session.Configuration;
import org.junit.Test;

/**
 * @author zh
 * @date 2021/4/13 16:58
 */
public class TypeAliasTest {

    @Test
    public void test(){
        Configuration configuration = new Configuration();
        configuration.getTypeAliasRegistry().registerAlias("User", User.class);
        // 注册整个包
        configuration.getTypeAliasRegistry().registerAliases("com.zhanghe.study.mybatis.model");
    }
}
