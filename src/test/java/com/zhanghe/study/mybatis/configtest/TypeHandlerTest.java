package com.zhanghe.study.mybatis.configtest;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.StringTypeHandler;
import org.junit.Test;

/**
 * @author zh
 * @date 2021/4/13 17:02
 */
public class TypeHandlerTest {

    @Test
    public void test(){
        Configuration configuration = new Configuration();
        configuration.getTypeHandlerRegistry().register(String.class, StringTypeHandler.class);
    }
}
