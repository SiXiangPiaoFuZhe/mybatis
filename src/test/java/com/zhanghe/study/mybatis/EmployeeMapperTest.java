package com.zhanghe.study.mybatis;

import com.zhanghe.study.mybatis.mapper.EmployeeMapper;
import com.zhanghe.study.mybatis.model.Employee;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.zhanghe.study.mybatis.Main.createFactory;

/**
 * @author zh
 * @date 2021/1/13 10:52
 */
public class EmployeeMapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void createSqlSessionFactory(){
        sqlSessionFactory = createFactory();
    }

    @Test
    public void getEmpAndDep() {
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);
        Employee emp = employeeMapper.getEmpAndDep(1);
//        System.out.println(emp);
        session.close();
    }


    @Test
    public void getEmployeeByCondition(){
        SqlSession session = sqlSessionFactory.openSession(true);
        Employee employee = new Employee();
//        employee.setId(1);
        employee.setName("%三%");
//        employee.setSalary(0);
        // mybatis为接口创建代理对象
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);
        List<Employee> emps = employeeMapper.getEmployeeByCondition(employee);
        System.out.println(emps);
        session.close();
    }

    @Test
    public void getEmployeeByConditionChoose(){
        SqlSession session = sqlSessionFactory.openSession(true);
        Employee employee = new Employee();
//        employee.setId(1);
//        employee.setName("%三%");
        employee.setSalary(100);
        // mybatis为接口创建代理对象
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);
        List<Employee> emps = employeeMapper.getEmployeeByConditionChoose(employee);
        System.out.println(emps);
        session.close();
    }

    @Test
    public void updateEmployeeBySet(){
        SqlSession session = sqlSessionFactory.openSession(true);
        Employee employee = new Employee();
        employee.setId(1);
//        employee.setName("%三%");
        employee.setSalary(100);
        // mybatis为接口创建代理对象
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);
        employeeMapper.updateEmployeeBySet(employee);
        session.close();
    }

}