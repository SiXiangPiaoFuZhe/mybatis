package com.zhanghe.study.mybatis;

import com.zhanghe.study.mybatis.mapper.UserMapper;
import com.zhanghe.study.mybatis.model.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.zhanghe.study.mybatis.Main.createFactory;

/**
 * @author zh
 * @date 2021/1/13 10:57
 */
public class UserMapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void createSqlSessionFactory(){
        sqlSessionFactory = createFactory();
    }
    /**
     * 测试mapper
     */
    @Test
    public void testMapper(){
        // 第一种方式
        SqlSession session = sqlSessionFactory.openSession(true);
        User user = session.selectOne("com.zhanghe.study.mybatis.mapper.UserMapper.selectUser",2);
        System.out.println(user);
        session.close();

        // 第二种方式
        session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user1 = userMapper.selectUser(2);
        System.out.println(user1);
        session.close();
    }

    /**
     * 测试查询
     */
    @Test
    public void testSelect(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user1 = userMapper.selectUser(2);
        System.out.println(user1);
        session.close();
    }

    /**
     * 测试多参数查询
     */
    @Test
    public void testSelect01(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user1 = userMapper.selectUserByIdAndName(2,"张三");
        System.out.println(user1);
        session.close();
    }

    /**
     * 测试多参数查询
     */
    @Test
    public void testSelect02(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user = new User();
        user.setId(8);
        user.setName("苍");
        user.setAccount(10);
        User user1 = userMapper.selectUserByCondition(user);
        System.out.println(user1);
        session.close();
    }

    /**
     * 测试集合参数查询
     */
    @Test
    public void testSelect03(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        List<Integer> ids= new ArrayList<>();
        ids.add(8);
        ids.add(9);
        List<User> users = userMapper.selectByIds(ids);
        System.out.println(users);
        session.close();
    }

    /**
     * 测试查询使用resultMap映射结果集
     */
    @Test
    public void testSelect04(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user1 = userMapper.selectUserReturnResultMap(8);
        System.out.println(user1);
        session.close();
    }

    /**
     * 测试插入
     */
    @Test
    public void testInsert(){
        SqlSession session = sqlSessionFactory.openSession(true);
        try{
            // mybatis为接口创建代理对象
            UserMapper userMapper = session.getMapper(UserMapper.class);
            User user = new User();
            user.setName("波多野结衣");
            user.setAccount(100);
            userMapper.addUser(user);
            System.out.println(user);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    /**
     * 测试返回自增主键值
     */
    @Test
    public void testInsertReturnGeneratedKey(){
        SqlSession session = sqlSessionFactory.openSession(true);
        try{
            // mybatis为接口创建代理对象
            UserMapper userMapper = session.getMapper(UserMapper.class);
            User user = new User();
            user.setName("苍井空");
            user.setAccount(10);
            userMapper.addUserReturnGeneratedKey(user);
            System.out.println(user);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    /**
     * 测试修改
     */
    @Test
    public void testUpdate(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user = userMapper.selectUser(7);
        user.setAccount(100);
        userMapper.updateUser(user);
        session.close();
    }

    /**
     * 测试删除
     */
    @Test
    public void testDelete(){
        SqlSession session = sqlSessionFactory.openSession(true);
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        userMapper.deleteUser(7);
        session.close();
    }


    /**
     * 测试二级缓存
     */
    @Test
    public void testTwoLevelCache(){
        SqlSession session = sqlSessionFactory.openSession();
        // mybatis为接口创建代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);
        User user = userMapper.selectUser(8);
        System.out.println(user);
        userMapper.updateUser(user);
        session.close();
        SqlSession session1 = sqlSessionFactory.openSession();

        UserMapper userMapper1 = session1.getMapper(UserMapper.class);
        User user1 = userMapper1.selectUser(8);
        System.out.println(user1);
        System.out.println(user == user1);

        session1.close();
    }


}