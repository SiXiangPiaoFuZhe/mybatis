package com.zhanghe.study.mybatis.annotest;

import com.zhanghe.study.mybatis.interceptors.Page;
import com.zhanghe.study.mybatis.interceptors.PageParam;
import com.zhanghe.study.mybatis.mapper.ClassesMapper;
import com.zhanghe.study.mybatis.mapper.StudentMapper;
import com.zhanghe.study.mybatis.model.Classes;
import com.zhanghe.study.mybatis.model.Student;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.zhanghe.study.mybatis.Main.createFactory;

/**
 * @author zh
 * @date 2021/4/14 18:32
 */
public class AnnoTest {
    private SqlSessionFactory sqlSessionFactory;
    private SqlSession session;

    @Before
    public void before(){
        sqlSessionFactory = createFactory();
        session = sqlSessionFactory.openSession(true);
    }

    @Test
    public void insertClass(){
        ClassesMapper mapper = session.getMapper(ClassesMapper.class);
        Classes classes = new Classes();
        classes.setName("东明01");
        mapper.insertClasses(classes);
    }

    @Test
    public void updateClass(){
        ClassesMapper mapper = session.getMapper(ClassesMapper.class);
        Classes classes = new Classes();
        classes.setId(1);
        classes.setName("东明修改了");
        mapper.updateClasses(classes);
    }

    @Test
    public void deleteClass(){
        ClassesMapper mapper = session.getMapper(ClassesMapper.class);
        mapper.deleteClasses(2);
    }

    @Test
    public void selectClass(){
        ClassesMapper classesMapper = session.getMapper(ClassesMapper.class);
        Classes classes = classesMapper.getById(1);
        System.out.println(classes);
    }

    @Test
    public void selectClass01(){
        ClassesMapper classesMapper = session.getMapper(ClassesMapper.class);
        Classes classes = classesMapper.get(1);
        System.out.println(classes);
    }

    @Test
    public void selectCollectionClass(){
        ClassesMapper classesMapper = session.getMapper(ClassesMapper.class);
        Classes classes = classesMapper.getColectionById(1);
        System.out.println(classes);
    }


    @Test
    public void insertStudent(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = new Student();
        student.setName("李四");
        Classes classes = new Classes();
        classes.setId(1);
        student.setClasses(classes);
        mapper.insertStudent(student);
    }

    @Test
    public void updateStudent(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = new Student();
        student.setId(2);
        student.setName("李四01");
        mapper.updateStudent(student);
    }

    @Test
    public void deleteStudent(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        mapper.deleteStudent(2);
    }

    @Test
    public void selectStudent(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = mapper.getById(1);
        System.out.println(student);
    }

    @Test
    public void selectStudentByClass(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        List<Student> students = mapper.getByClassIdByPager(1,1,1);
        System.out.println(students);
    }

    @Test
    public void selectStudentByClassPage(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        PageParam pageParam = new PageParam(1,1);
        Page<Student> students = mapper.getByClassIdPage(1,pageParam);

        System.out.println(students);
        System.out.println(students.getTotalCount());
    }

    @Test
    public void selectStudent01(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = mapper.get(1);
        System.out.println(student);
    }

    @Test
    public void selectAssociationStudent(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = mapper.getAssociationById(1);
        System.out.println(student);
    }

    @Test
    public void selectStudentByProvider(){
        StudentMapper mapper = session.getMapper(StudentMapper.class);
        Student student = mapper.getByIdUseProvider(1);
        System.out.println(student);
    }

    @After
    public void after(){
        if(session != null){
            session.close();
        }
    }
}
