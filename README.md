# mybatis

## 介绍
mybatis学习

## 项目说明
使用了三种技术，依次进行迭代
一为 单纯的mybatis技术 使用的配置文件为mybatis-config.xml
二为 mybatis和spring进行整合 使用的是mybatis-config-spring.xml以及applicationContext.xml
三为 mybatis和spring、通用Mapper进行整合 使用的是mybatis-config-spring.xml以及applicationContext-tk.xml
### 单纯的使用mybatis
#### 所需依赖
```xml
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.5</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.48</version>
        </dependency>
```
#### 测试类
DepartmentMapperTest
EmployeeMapperTest
UserMapperTest

### mybatis和spring进行整合
#### 所需依赖
在原来的基础上增加spring的依赖
```xml
        <!-- mybatis整合spring -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.3</version>
        </dependency>

        <!-- spring 相关依赖 -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>4.3.29.RELEASE</version>
        </dependency>
```

#### 测试
MybatisSpringTest

### 继续整合通用mapper
#### 所需依赖
在原来的基础上增加通用Mapper的依赖
```xml
        <!-- mybatis功能增强  通用mapper -->
        <dependency>
            <groupId>tk.mybatis</groupId>
            <artifactId>mapper</artifactId>
            <version>4.1.0</version>
        </dependency>
```
#### 测试
TkMapperTest

#### 使用
##### Mapper接口调整
在使用通用Mapper时，XxxMapper需要继承通用的Mapper接口

##### 实体类调整
在实体类上需要使用@Table来定义表名(默认使用的规则是类名首字母小写)
使用@Column注解来指定字段所对应的列名

再根据主键查询时，需要在主键列对应的字段上加上@Id,如果没有使用@Id的话，会将所有字段都作为主键，作为联合主键
